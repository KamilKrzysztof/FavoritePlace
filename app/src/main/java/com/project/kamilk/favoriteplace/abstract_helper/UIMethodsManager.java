package com.project.kamilk.favoriteplace.abstract_helper;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.EditText;

import com.project.kamilk.favoriteplace.constans.Contracts;
import com.project.kamilk.favoriteplace.constans.Contracts.MenuItemConstants;

public abstract class UIMethodsManager {

    public static boolean LIST_ELEMENT_UP = true;

    public static void makeUIEditable_Single(EditText editText){
        if (MenuItemConstants.menuDetailItemVisible){
            editText.setEnabled(true);
        } else {
            editText.setEnabled(false);
        }
    }

    public static void MoveTheListItem(Context context, Cursor cursor, int position) {

        int positionNext;

        if (position != -1 && position < cursor.getCount()) {
            cursor.moveToPosition(position);
        } else return;

        if(LIST_ELEMENT_UP){
            positionNext = position - 1;
        } else {
            positionNext = position + 1;
        }


        long mainId = getId(cursor);
        ContentValues mainContentValues = getContentValues(cursor);

        if (positionNext != -1 && positionNext < cursor.getCount()) {
            cursor.moveToPosition(positionNext);
        } else return;

        long newId = getId(cursor);
        ContentValues nextContentValues = getContentValues(cursor);

        Uri mainUri = ContentUris.withAppendedId(Contracts.ProviderConstants.CONTENT_URI, mainId);
        Uri nextUri = ContentUris.withAppendedId(Contracts.ProviderConstants.CONTENT_URI, newId);

        context.getContentResolver().update(mainUri, nextContentValues, null, null);
        context.getContentResolver().update(nextUri, mainContentValues, null, null);
    }


    private static long getId(Cursor cursor) {
        int columnIndexID = cursor.getColumnIndex(Contracts.DatabaseConstants._ID);
        return cursor.getLong(columnIndexID);
    }


    private static ContentValues getContentValues(Cursor cursor) {
        int titleColumnIndex = cursor.getColumnIndex(Contracts.DatabaseConstants.COLUMN_TITLE);
        int noteColumnIndex = cursor.getColumnIndex(Contracts.DatabaseConstants.COLUMN_NOTE);
        int addressColumnIndex = cursor.getColumnIndex(Contracts.DatabaseConstants.COLUMN_ADDRESS);
        int latitudeColumnIndex = cursor.getColumnIndex(Contracts.DatabaseConstants.COLUMN_LATITUDE);
        int longitudeColumnIndex = cursor.getColumnIndex(Contracts.DatabaseConstants.COLUMN_LONGITUDE);

        ContentValues contentValues = new ContentValues();
        contentValues.put(Contracts.DatabaseConstants.COLUMN_TITLE, cursor.getString(titleColumnIndex));
        contentValues.put(Contracts.DatabaseConstants.COLUMN_NOTE, cursor.getString(noteColumnIndex));
        contentValues.put(Contracts.DatabaseConstants.COLUMN_ADDRESS, cursor.getString(addressColumnIndex));
        contentValues.put(Contracts.DatabaseConstants.COLUMN_LATITUDE, cursor.getDouble(latitudeColumnIndex));
        contentValues.put(Contracts.DatabaseConstants.COLUMN_LONGITUDE, cursor.getDouble(longitudeColumnIndex));

        return contentValues;
    }
}
