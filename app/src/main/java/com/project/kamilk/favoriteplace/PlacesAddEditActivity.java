package com.project.kamilk.favoriteplace;

import android.Manifest;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentSender;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.project.kamilk.favoriteplace.abstract_helper.DatabaseMethodsManager;
import com.project.kamilk.favoriteplace.abstract_helper.LocationMethodsManager;
import com.project.kamilk.favoriteplace.abstract_helper.OtherMethodsManager;
import com.project.kamilk.favoriteplace.abstract_helper.UIMethodsManager;
import com.project.kamilk.favoriteplace.constans.Contracts.IntentServiceConstants;
import com.project.kamilk.favoriteplace.constans.Contracts.MenuItemConstants;
import com.project.kamilk.favoriteplace.constans.Contracts.ProviderConstants;
import com.project.kamilk.favoriteplace.constans.Contracts.DatabaseConstants;
import com.project.kamilk.favoriteplace.interfaces.ContentProviderMethodHelper;

public class PlacesAddEditActivity extends AppCompatActivity implements
        ContentProviderMethodHelper,
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final String PLACE_ARG_URI_KEY = "place_arg_uri_key";
    public static final String PLACE_ARG_ID_KEY = "place_arg_id_key";
    public static final int REQUEST_PERMISSION_REQUEST_CODE = 21;
    public static final int REQUEST_CHECK_SETTINGS = 20;
    public static final String LOCATION_ADDRESS_KEY = "location-address";

    //UI Widgets
    private EditText placeTitle_et;
    private EditText placeNote_et;
    private EditText placeAddress_et;
    private TextView placeLongitude_tv;
    private TextView placeLatitude_tv;

    //Labels
    private String title;
    private String note;
    private String address;
    private double longitude;
    private double latitude;

    private long takenId;
    private Uri currentUri;

    private boolean mRequestingLocationUpdates;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private SettingsClient mSettingsClient;
    private Location currentLocation;
    private AddressResultReceiver mResultReceiver;
    private String mAddressOutput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_add_edit);

        //Locate the UI Widgets
        placeTitle_et = (EditText) findViewById(R.id.place_title_et);
        placeNote_et = (EditText) findViewById(R.id.place_note_et);
        placeAddress_et = (EditText) findViewById(R.id.place_address_et);
        placeLatitude_tv = (TextView) findViewById(R.id.place_latitude_tv);
        placeLongitude_tv = (TextView) findViewById(R.id.place_longitude_tv);
        ImageView showAddress_img = (ImageView) findViewById(R.id.show_address_image);

        takenId = getIntent().getLongExtra(PLACE_ARG_ID_KEY, 0);
        String takenConvertedUri = getIntent().getStringExtra(PLACE_ARG_URI_KEY);
        currentUri = Uri.parse(takenConvertedUri);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        mResultReceiver = new AddressResultReceiver(new android.os.Handler());

        editableOptionsUI();

        mAddressOutput = "";
        updateValuesFromBundle(savedInstanceState);

        mLocationRequest = LocationMethodsManager.createLocationRequest(mLocationRequest);
        mLocationSettingsRequest = LocationMethodsManager.createLocationSettingsRequest();
        createLocationCallback();

        setRefreshLocationOnTheScreen();

        showAddress_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchAddressButtonHander();
            }
        });

        getLoaderManager().initLoader(ProviderConstants.LOADER_ID, null, this);
    }

    private void setRefreshLocationOnTheScreen() {
        if (takenId == 0) {
            mRequestingLocationUpdates = true;
        } else {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            if (savedInstanceState.keySet().contains(LOCATION_ADDRESS_KEY)) {
                mAddressOutput = savedInstanceState.getString(LOCATION_ADDRESS_KEY);
                displayAddressOutput();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if ((OtherMethodsManager.checkPermission
                (this, Manifest.permission.ACCESS_FINE_LOCATION))
                && mRequestingLocationUpdates) {
            startLocationUpdate();
        } else if (!OtherMethodsManager.checkPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermission();
        }
    }


    //Location
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                currentLocation = locationResult.getLastLocation();
                updateLocationUI(); //wysyłac to w bundlu
            }
        };
    }

    private void updateLocationUI() {
        if (currentLocation != null) {
            longitude = currentLocation.getLongitude();
            latitude = currentLocation.getLatitude();

            placeLatitude_tv.setText(String.valueOf(latitude));
            placeLongitude_tv.setText(String.valueOf(longitude));
        }
    }

    private void refreshLocation() {
        mRequestingLocationUpdates = true;
        startLocationUpdate();
    }

    private void stopLocationUpdates() {
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }

    @SuppressWarnings("MissingPermission")
    private void startLocationUpdate() {
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        mFusedLocationProviderClient.requestLocationUpdates(
                                mLocationRequest,
                                mLocationCallback,
                                Looper.myLooper());

                        updateLocationUI();
                    }
                }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(PlacesAddEditActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
                mRequestingLocationUpdates = false;
                updateLocationUI();
            }
        });
    }
    //Location end

    //Address
    private class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            mAddressOutput = resultData.getString(IntentServiceConstants.RESULT_DATA_KEY);
            displayAddressOutput();

            if (resultCode == IntentServiceConstants.SUCCESS_RESULT) {
                Toast.makeText(PlacesAddEditActivity.this,
                        getString(R.string.address_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected void startIntentService() {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(IntentServiceConstants.RECEIVER, mResultReceiver);
        intent.putExtra(IntentServiceConstants.LOCATION_DATA_EXTRA, currentLocation);
        startService(intent);
    }


    @SuppressWarnings("MissingPermission")
    private void fetchAddressButtonHander() {
        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        currentLocation = location;

                        // In some rare cases the location returned can be null
                        if (currentLocation == null) {
                            Log.i("location error", "location = null");
                        }

                        if (!Geocoder.isPresent()) {
                            Toast.makeText(PlacesAddEditActivity.this,
                                    R.string.no_geocoder_available,
                                    Toast.LENGTH_LONG).show();
                            return;
                        }
                        startIntentService();
                    }
                });
    }

    private void displayAddressOutput() {
        placeAddress_et.setText(mAddressOutput);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(LOCATION_ADDRESS_KEY, mAddressOutput);
        super.onSaveInstanceState(savedInstanceState);
    }

    //Address end

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                MenuItemConstants.changeIcons_List(this);
            case R.id.item_edit:
                MenuItemConstants.changeIcons_Detail(this);
                editableOptionsUI();
                break;
            case R.id.item_delete:
                delete();
                MenuItemConstants.changeIcons_List(this);
                break;
            case R.id.item_refresh_location:
                refreshLocation();
                break;
            case R.id.item_accept:
                if (takenId != 0) {
                    update();
                } else {
                    insert();
                }
                MenuItemConstants.changeIcons_List(this);
                break;
            case R.id.item_cancel:
                if (takenId != 0) {
                    stopLocationUpdates();
                    MenuItemConstants.changeIcons_Edit(this);
                    editableOptionsUI();
                } else {
                    MenuItemConstants.changeIcons_List(this);
                }
                break;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (!MenuItemConstants.menuEditItemVisible) {
            MenuItem menuEditItemE = menu.findItem(R.id.item_edit);
            MenuItem menuEditItemD = menu.findItem(R.id.item_delete);
            menuEditItemE.setVisible(false);
            menuEditItemD.setVisible(false);
        }
        if (!MenuItemConstants.menuDetailItemVisible) {
            MenuItem menuNewItemR = menu.findItem(R.id.item_refresh_location);
            MenuItem menuNewItemA = menu.findItem(R.id.item_accept);
            MenuItem menuNewItemC = menu.findItem(R.id.item_cancel);
            menuNewItemA.setVisible(false);
            menuNewItemC.setVisible(false);
            menuNewItemR.setVisible(false);

        }
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = new String[]{
                DatabaseConstants._ID,
                DatabaseConstants.COLUMN_TITLE,
                DatabaseConstants.COLUMN_NOTE,
                DatabaseConstants.COLUMN_ADDRESS,
                DatabaseConstants.COLUMN_LONGITUDE,
                DatabaseConstants.COLUMN_LATITUDE
        };

        return new CursorLoader(this,
                currentUri,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        if (cursor.moveToFirst()) {
            int titleColumnIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_TITLE);
            int noteColumnIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_NOTE);
            int latitudeColumnIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_LATITUDE);
            int longitudeColumnIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_LONGITUDE);
            int addressColumnIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_ADDRESS);

            String title = cursor.getString(titleColumnIndex);
            String note = cursor.getString(noteColumnIndex);
            double latitude = cursor.getDouble(latitudeColumnIndex);
            double longitude = cursor.getDouble(longitudeColumnIndex);
            String address = cursor.getString(addressColumnIndex);

            placeTitle_et.setText(title);
            placeNote_et.setText(note);
            placeAddress_et.setText(address);
            placeLongitude_tv.setText(String.valueOf(latitude));
            placeLatitude_tv.setText(String.valueOf(longitude));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        placeTitle_et.setText("");
        placeNote_et.setText("");
        placeAddress_et.setText("");
        placeLongitude_tv.setText("");
        placeLatitude_tv.setText("");
    }

    @Override
    public void insert() {
        takeTextFromUI();
        DatabaseMethodsManager.insertElement(this, ProviderConstants.CONTENT_URI, contentValuesHelper());
    }

    @Override
    public void update() {
        takeTextFromUI();
        DatabaseMethodsManager.updateElement(this, currentUri, contentValuesHelper());
    }

    @Override
    public void delete() {
        takeTextFromUI();
        DatabaseMethodsManager.deleteSingleElement(this, currentUri);
    }

    @Override
    public ContentValues contentValuesHelper() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseConstants.COLUMN_TITLE, title);
        contentValues.put(DatabaseConstants.COLUMN_NOTE, note);
        contentValues.put(DatabaseConstants.COLUMN_ADDRESS, address);
        contentValues.put(DatabaseConstants.COLUMN_LONGITUDE, longitude);
        contentValues.put(DatabaseConstants.COLUMN_LATITUDE, latitude);
        return contentValues;
    }

    private void takeTextFromUI() {

        String UIFieldTitle = placeTitle_et.getText().toString();
        this.title = checkTheUIStringField(UIFieldTitle);

        String UIFieldNote = placeNote_et.getText().toString();
        this.note = checkTheUIStringField(UIFieldNote);

        String UIFieldAddress = placeAddress_et.getText().toString();
        this.address = checkTheUIStringField(UIFieldAddress);

        String UIFieldLongitude = placeLongitude_tv.getText().toString();
        this.longitude = checkTheUIDoubleField(UIFieldLongitude);

        String UIFieldLatitude = placeLatitude_tv.getText().toString();
        this.latitude = checkTheUIDoubleField(UIFieldLatitude);
    }

    private String checkTheUIStringField(String uiField) {
        String mainValue;
        if (uiField.length() == 0) {
            mainValue = "empty";
        } else {
            mainValue = uiField;
        }
        return mainValue;
    }

    private double checkTheUIDoubleField(String uiField) {
        double mainValue;
        if (uiField.length() == 0) {
            mainValue = 0;
        } else {
            mainValue = Double.valueOf(uiField);
        }
        return mainValue;
    }

    private boolean editableOptionsUI() {
        UIMethodsManager.makeUIEditable_Single(placeTitle_et);
        UIMethodsManager.makeUIEditable_Single(placeNote_et);
        UIMethodsManager.makeUIEditable_Single(placeAddress_et);
        return true;
    }

    //Permissions

    private void requestPermission() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale
                (this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) { //powinien dostarczyc uzasadnienia
            OtherMethodsManager.showSnackbar(this, R.string.permission_rationale, android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestFineLocationPermission();
                }
            });

        } else {
            requestFineLocationPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Toast.makeText(this, "User interaction - cancelled", Toast.LENGTH_SHORT).show();
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdate();
                Toast.makeText(this, "permission granded", Toast.LENGTH_SHORT).show();
            }
        } else {
            OtherMethodsManager.showSnackbar(this,
                    R.string.permission_danied_explantation,
                    R.string.settings,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });
        }
    }

    private void requestFineLocationPermission() {
        ActivityCompat.requestPermissions(
                PlacesAddEditActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSION_REQUEST_CODE
        );
    }


}



