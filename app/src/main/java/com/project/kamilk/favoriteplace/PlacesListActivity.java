package com.project.kamilk.favoriteplace;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.project.kamilk.favoriteplace.abstract_helper.DatabaseMethodsManager;
import com.project.kamilk.favoriteplace.constans.Contracts.ProviderConstants;
import com.project.kamilk.favoriteplace.constans.Contracts.DatabaseConstants;
import com.project.kamilk.favoriteplace.constans.Contracts.MenuItemConstants;

import static com.project.kamilk.favoriteplace.PlacesAddEditActivity.PLACE_ARG_ID_KEY;


public class PlacesListActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private PlacesCursorAdapter placesCursorAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_catalog);

        ListView placesListView = (ListView) findViewById(R.id.places_listview);

        placesCursorAdapter = new PlacesCursorAdapter(this, null); // empty adapter
        View emptyView = findViewById(R.id.empty_view);
        placesListView.setEmptyView(emptyView);

        placesListView.setAdapter(placesCursorAdapter);

        placesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                chooseStructure(id);
            }
        });

        FloatingActionButton fabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseStructure(0);
            }
        });
        getLoaderManager().initLoader(ProviderConstants.LOADER_ID, null, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.item_delete_all:
                deleteAll();
                break;
        }
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (MenuItemConstants.menuListItemVisible) {
            MenuItem menuListItem = menu.findItem(R.id.item_delete_all);
            menuListItem.setVisible(true);
        }
        return true;
    }


    private void deleteAll() {
        DatabaseMethodsManager.deleteAllElements(this, ProviderConstants.CONTENT_URI);
    }

    private void chooseStructure(long id) {

        Uri currentPlaceUri = ContentUris.withAppendedId(ProviderConstants.CONTENT_URI, id);
        String convertedCurrentUri = currentPlaceUri.toString();

        if (id != 0) {
            MenuItemConstants.changeIcons_Edit(this);

            Intent intent = new Intent(PlacesListActivity.this, PlacesAddEditActivity.class);
            intent.putExtra(PlacesAddEditActivity.PLACE_ARG_URI_KEY, convertedCurrentUri);
            intent.putExtra(PlacesAddEditActivity.PLACE_ARG_ID_KEY, id);
            startActivity(intent);
        } else {
            MenuItemConstants.changeIcons_Detail(this);

            Intent intent = new Intent(PlacesListActivity.this, PlacesAddEditActivity.class);
            intent.putExtra(PlacesAddEditActivity.PLACE_ARG_URI_KEY, convertedCurrentUri);
            intent.putExtra(PLACE_ARG_ID_KEY, id);
            startActivity(intent);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = new String[]{
                DatabaseConstants._ID,
                DatabaseConstants.COLUMN_TITLE,
                DatabaseConstants.COLUMN_NOTE,
                DatabaseConstants.COLUMN_ADDRESS,
                DatabaseConstants.COLUMN_LONGITUDE,
                DatabaseConstants.COLUMN_LATITUDE
        };
        return new CursorLoader(this,
                ProviderConstants.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        placesCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        placesCursorAdapter.swapCursor(null);
    }
}
