package com.project.kamilk.favoriteplace;


import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.project.kamilk.favoriteplace.constans.Contracts.IntentServiceConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FetchAddressIntentService extends IntentService {

    private static final String TAG = "FetchAddressIS";
    private ResultReceiver mResultReceiver;

    public FetchAddressIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String errorMessage = "";

        mResultReceiver = intent.getParcelableExtra(IntentServiceConstants.RECEIVER);

        Location location = intent.getParcelableExtra(IntentServiceConstants.LOCATION_DATA_EXTRA);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addressList = null;

        if (location == null){
            errorMessage = "brak lokacji";
            deliverResultToReceiver(IntentServiceConstants.FAILURE_RESULT, errorMessage);
            return;
        }

        try {
            addressList = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);
            Log.i(TAG, String.valueOf(location.getLatitude()));

        } catch (IOException ioException) { // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) { // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " +
                    location.getLongitude(), illegalArgumentException);

        } if (addressList ==  null && addressList.size() == 0){
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
            deliverResultToReceiver(IntentServiceConstants.FAILURE_RESULT, errorMessage);
        } else{
            Address address = addressList.get(0);
            ArrayList<String> addressFragment = new ArrayList<>();

            for (int i=0; i<=address.getMaxAddressLineIndex(); i++){
                addressFragment.add(address.getAddressLine(i));
            }

            Log.i(TAG, getString(R.string.address_found));
            deliverResultToReceiver(IntentServiceConstants.SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.separator"), addressFragment));
        }

    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(IntentServiceConstants.RESULT_DATA_KEY, message);
        mResultReceiver.send(resultCode, bundle);
    }
}
