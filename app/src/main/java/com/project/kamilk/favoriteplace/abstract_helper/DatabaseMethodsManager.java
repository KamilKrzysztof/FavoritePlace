package com.project.kamilk.favoriteplace.abstract_helper;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

public abstract class DatabaseMethodsManager {

    public static void insertElement(Context context, Uri uri, ContentValues values){
        context.getContentResolver().insert(uri, values);
    }
    public static void updateElement(Context context, Uri uri, ContentValues values){
        context.getContentResolver().update(uri, values, null, null);
    }
    public static void deleteSingleElement(Context context, Uri uri){
        context.getContentResolver().delete(uri, null, null);
    }
    public static void deleteAllElements(Context context, Uri uri){
        context.getContentResolver().delete(uri, null, null);
    }
}
