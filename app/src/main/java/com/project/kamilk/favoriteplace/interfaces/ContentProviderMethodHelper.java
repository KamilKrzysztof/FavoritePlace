package com.project.kamilk.favoriteplace.interfaces;


import android.content.ContentValues;

public interface ContentProviderMethodHelper {
    void insert();
    void update();
    void delete();
    ContentValues contentValuesHelper();
}
