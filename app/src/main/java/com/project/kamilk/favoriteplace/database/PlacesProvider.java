package com.project.kamilk.favoriteplace.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.project.kamilk.favoriteplace.constans.Contracts.ProviderConstants;
import com.project.kamilk.favoriteplace.constans.Contracts.DatabaseConstants;

import static com.project.kamilk.favoriteplace.constans.Contracts.ProviderConstants.PLACES;
import static com.project.kamilk.favoriteplace.constans.Contracts.ProviderConstants.PLACE_ID;

public class PlacesProvider extends ContentProvider {

    private DbHelper dbHelper;

    private static final UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        mUriMatcher.addURI(ProviderConstants.PLACE_AUTORITHY, ProviderConstants.PLACE_PATH, PLACES);
        mUriMatcher.addURI(ProviderConstants.PLACE_AUTORITHY, ProviderConstants.PLACE_PATH + "/#", ProviderConstants.PLACE_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor;
        projection = new String[]{
                DatabaseConstants._ID,
                DatabaseConstants.COLUMN_TITLE,
                DatabaseConstants.COLUMN_NOTE,
                DatabaseConstants.COLUMN_ADDRESS,
                DatabaseConstants.COLUMN_LONGITUDE,
                DatabaseConstants.COLUMN_LATITUDE
        };

        int match = mUriMatcher.match(uri);
        switch (match) {
            case PLACES:
                cursor = database.query(DatabaseConstants.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case PLACE_ID:
                selection = DatabaseConstants._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };
                cursor = database.query(DatabaseConstants.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);

                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        int match = mUriMatcher.match(uri);
        switch (match) {
            case PLACES:
                long newRowId = database.insert(DatabaseConstants.TABLE_NAME, null, contentValues);
                if (newRowId == -1) {
                    Log.e("Insert provider error", "Failed to insert row for " + uri);
                    return null;
                }
                getContext().getContentResolver().notifyChange(uri, null);
                Uri newRowUri = ContentUris.withAppendedId(uri, newRowId);
                return newRowUri;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        int deleteRow;
        int match = mUriMatcher.match(uri);
        switch (match) {
            case PLACES:
                deleteRow = database.delete(DatabaseConstants.TABLE_NAME, selection, selectionArgs);
                break;
            case PLACE_ID:
                selection = DatabaseConstants._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                deleteRow = database.delete(DatabaseConstants.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException();
        }
        if (deleteRow != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deleteRow;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {

        int match = mUriMatcher.match(uri);
        switch (match) {
            case PLACES:
                return updatePlace(uri, contentValues, selection, selectionArgs);

            case PLACE_ID:
                selection = DatabaseConstants._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };
                return updatePlace(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException();
        }
    }

    private int updatePlace(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        int updatedRows;
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        updatedRows = database.update(DatabaseConstants.TABLE_NAME, contentValues, selection, selectionArgs);
        if (updatedRows != -0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updatedRows;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case PLACES:
                return ProviderConstants.LIST_TYPE_MANY;
            case PLACE_ID:
                return ProviderConstants.LIST_TYPE_ONE;
            default:
                throw new IllegalArgumentException();
        }
    }
}
